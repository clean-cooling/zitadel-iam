Zitadel
====

Zitadel is an Identity and Access Management (IAM) provider that we use to provide Single Sign-On (SSO) across all 
of the various Clean Cooling applications and services.

See https://zitadel.com/ for more information.

## Updating

To update the package, simply rebuild the app on Digital Ocean, which will pull the latest Zitadel Docker image.

Click **"Actions"** and the **"Force Rebuild and Deploy"** (cache clearing NOT needed).

